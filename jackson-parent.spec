Name:          jackson-parent
Version:       2.16
Release:       2%{?dist}
Summary:       Parent pom for all Jackson components
License:       Apache-2.0

URL:           https://github.com/FasterXML/jackson-parent
Source0:       %{url}/archive/%{name}-%{version}.tar.gz
# jackson-parent package don't include the license file
# reported @ https://github.com/FasterXML/jackson-parent/issues/1
Source1:       http://www.apache.org/licenses/LICENSE-2.0.txt

BuildRequires:  maven-local
BuildRequires:  mvn(com.fasterxml:oss-parent:pom:) >= 56

BuildArch:      noarch
%if 0%{?fedora}
ExclusiveArch:  %{java_arches} noarch
%endif

%description
Project for parent pom for all Jackson components.

%package -n pki-%{name}
Summary: Parent pom for all Jackson components
Obsoletes: %{name} < %{version}-%{release}
Conflicts: %{name} < %{version}-%{release}
Provides: %{name} = %{version}-%{release}

%description -n pki-%{name}
Project for parent pom for all Jackson components.

%prep
%setup -q -n %{name}-%{name}-%{version}

cp -p %{SOURCE1} LICENSE
sed -i 's/\r//' LICENSE

%build
%mvn_build -j

%install
%mvn_install

%files -n pki-%{name} -f .mfiles
%doc README.md
%license LICENSE

%changelog
* Tue Dec 05 2023 Red Hat PKI Team <rhcs-maint@redhat.com> - 2.16-2
- Fix fasterxml-oss-parent dependency

* Tue Dec 05 2023 Red Hat PKI Team <rhcs-maint@redhat.com> - 2.16-1
- Rebase to version 2.16

* Tue Nov 08 2022 Chris Kelley <ckelley@redhat.com> - 2.14-1
- Update to version 2.14
- Resolves: #2070122

* Mon Aug 09 2021 Mohan Boddu <mboddu@redhat.com> - 2.11-9
- Rebuilt for IMA sigs, glibc 2.34, aarch64 flags
  Related: rhbz#1991688

* Mon Apr 26 2021 Red Hat PKI Team <rhcs-maint@redhat.com> - 2.11-8
- Add Conflicts

* Fri Apr 23 2021 Red Hat PKI Team <rhcs-maint@redhat.com> - 2.11-7
- Add Obsoletes

* Fri Apr 23 2021 Red Hat PKI Team <rhcs-maint@redhat.com> - 2.11-6
- Rename subpackages to pki-jackson

* Fri Apr 16 2021 Mohan Boddu <mboddu@redhat.com> - 2.11-5
- Rebuilt for RHEL 9 BETA on Apr 15th 2021. Related: rhbz#1947937

* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 2.11-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Tue Jul 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 2.11-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Fri Jul 10 2020 Jiri Vanek <jvanek@redhat.com> - 2.11-2
- Rebuilt for JDK-11, see https://fedoraproject.org/wiki/Changes/Java11

* Mon May 25 2020 Fabio Valentini <decathorpe@gmail.com> - 2.11-1
- Update to version 2.11.

* Wed Jan 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 2.10-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Thu Oct 03 2019 Fabio Valentini <decathorpe@gmail.com> - 2.10-1
- Update to version 2.10.

* Thu Jul 25 2019 Fedora Release Engineering <releng@fedoraproject.org> - 2.9.1.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Wed Feb 06 2019 Mat Booth <mat.booth@redhat.com> - 2.9.1.2-1
- Update to latest upstream release

* Fri Feb 01 2019 Fedora Release Engineering <releng@fedoraproject.org> - 2.9.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 2.9.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 2.9.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Jan 11 2018 Mat Booth <mat.booth@redhat.com> - 2.9.1-1
- Update to latest upstream release

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 2.7-3.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 2.7-2.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Aug 22 2016 gil cattaneo <puntogil@libero.it> 2.7-1.1
- update to 2.7-1

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 2.6.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Mon Sep 28 2015 gil cattaneo <puntogil@libero.it> 2.6.2-1
- update to 2.6.2

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat Jan 31 2015 gil cattaneo <puntogil@libero.it> 2.5-1
- update to 2.5

* Wed Jul 02 2014 gil cattaneo <puntogil@libero.it> 2.4.1-1
- initial rpm
